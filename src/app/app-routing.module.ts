import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UploadFileComponent } from './views/upload-file/upload-file.component';
import { ValidateFileComponent } from './views/validate-file/validate-file.component';
import { RulersConfigComponent } from './views/rulers-config/rulers-config.component';
import { UserManagementComponent } from './views/user-management/user-management.component';
import { NavbarComponent } from './components/navbar/navbar.component';

const routes: Routes = [
  {
    path: 'login',component: LoginComponent
  },
  {
    path: 'data-harvor-web',component: NavbarComponent,
    children:[
      {path: 'home',component: HomeComponent },
      {path: 'upload-file',component: UploadFileComponent },
      {path: 'validate-file',component: ValidateFileComponent },
      {path: 'ruler-config',component: RulersConfigComponent },
      {path: 'user-managment',component: UserManagementComponent }
    ]
  },
  { path: "", redirectTo: "login", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
