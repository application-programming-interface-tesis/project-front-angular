import { NgModule } from '@angular/core';
import { BrowserModule, platformBrowser } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AccessDeneidComponent } from './components/access-deneid/access-deneid.component';
import { UploadFileComponent } from './views/upload-file/upload-file.component';
import { ValidateFileComponent } from './views/validate-file/validate-file.component';
import { RulersConfigComponent } from './views/rulers-config/rulers-config.component';
import { UserManagementComponent } from './views/user-management/user-management.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { PickListModule } from 'primeng/picklist';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { AppMenuComponent } from './components/app-menu/app-menu.component';
import { SidebarModule } from 'primeng/sidebar';
import { MenubarModule } from 'primeng/menubar';
import { HomeComponent } from './components/home/home.component';
import { SideBarLeftComponent } from './components/side-bar-left/side-bar-left.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarModule } from 'primeng/toolbar';
import { MessageService } from 'primeng/api';
import { FileUploadModule } from 'primeng/fileupload';
import { PanelModule } from 'primeng/panel';

@NgModule({
  declarations: [    
    AppComponent,
    LoginComponent,
    AccessDeneidComponent,
    UploadFileComponent,
    ValidateFileComponent,
    RulersConfigComponent,
    UserManagementComponent,
    AppMenuComponent,
    HomeComponent,
    SideBarLeftComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PasswordModule,
    InputTextModule,
    ButtonModule,
    CardModule,
    TableModule,
    ToastModule,
    DialogModule,
    DropdownModule,
    PickListModule,
    CalendarModule,
    SidebarModule,
    MenubarModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToolbarModule,
    FileUploadModule,
    PanelModule
  ],
  providers: [MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
