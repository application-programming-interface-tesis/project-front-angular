import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessDeneidComponent } from './access-deneid.component';

describe('AccessDeneidComponent', () => {
  let component: AccessDeneidComponent;
  let fixture: ComponentFixture<AccessDeneidComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccessDeneidComponent]
    });
    fixture = TestBed.createComponent(AccessDeneidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
