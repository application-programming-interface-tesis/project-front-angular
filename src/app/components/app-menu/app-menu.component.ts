import { Component } from '@angular/core';

@Component({
  selector: 'app-app-menu',
  templateUrl: './app-menu.component.html',
  styleUrls: ['./app-menu.component.css']
})
export class AppMenuComponent {

  displaySidebar = false;

  items = [
    {
      label: 'Validar',
      icon: 'pi pi-home',
    },
    {
      label: 'Cargar Archivo',
      icon: 'pi pi-th-large',
    },
    {
      label: 'Configuracion de reglas',
      icon: 'pi pi-cog',
    },
    {
      label: 'Gestion Usuarios',
      icon: 'pi pi-envelope',
    },
  ];

  toggleSidebar() {
    this.displaySidebar = !this.displaySidebar;
  }
}
