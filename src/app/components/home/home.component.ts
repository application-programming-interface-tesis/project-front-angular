import { Component, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { SideBarLeftComponent } from '../side-bar-left/side-bar-left.component';
import { Sidebar } from 'primeng/sidebar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  items: MenuItem[] | undefined;
  visibleSideBar: boolean = false
  @ViewChild('sidebarRef') sidebarRef!: Sidebar;

  closeCallback(e: Event): void {
      this.sidebarRef.close(e);
  }

  sidebarVisible: boolean = false;
  constructor(){

  }

  ngOnInit() {
    this.items = [
      {
        label: 'Show Menu',
        icon: 'pi pi-bars'
      }
    ]
  }


}
