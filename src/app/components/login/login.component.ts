import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SignIn } from './model/sign-in';
import { Route, Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { LoginResponse } from './model/login-response';
import { MessageService } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public signInForm!: FormGroup

  constructor(private fb: FormBuilder, private router:Router, private loginService:LoginService, private message: MessageService){
    
  }

  ngOnInit(): void {
    this.signInForm = new SignIn().formBuilder(this.fb)
    
  }

  signIn(){
    this.loginService.validateUser(this.signInForm).subscribe({         
      next:(response:LoginResponse)=>{
        if (response.code == 0) {   
          this.messageToast('success',response.message);
          localStorage.setItem("username",this.signInForm.value.user_mail);
          this.router.navigate(['data-harvor-web/home'])          
        } else {
          this.messageToast('warn',response.message);       
        }
      },error:(error)=>{
        this.message.add({ severity: 'error', summary: 'Error', detail: error });   
      }
    });
  }

  changePassword(){
    console.log("change password");
    
  }

  messageToast(option:string, content:string){
    switch (option) {
      case 'success':
        this.message.add({ severity: 'success', summary: 'Success', detail: content });   
        break;
      case 'warn':
        this.message.add({ severity: 'warn', summary: 'Warn', detail: content });   
        break;
      case 'error':
        this.message.add({ severity: 'error', summary: 'Error', detail: content });   
        break;
      default:
        break;
    }
  }
}
