import { FormBuilder, FormGroup, Validators } from "@angular/forms";

export class SignIn  {
    user_mail: string;
    user_password: string;

    constructor() {
        this.user_mail = "";
        this.user_password = "";
    }

    newObject(payload: any): void {
        this.user_mail = payload["user_mail"]
        this.user_password = payload["user_password"]
    }

    formBuilder(fb: FormBuilder): FormGroup {
        return fb.group({
            user_mail: [this.user_mail, Validators.compose([Validators.required])],
            user_password: [this.user_password, Validators.compose([Validators.required])]
        })
    }
}
