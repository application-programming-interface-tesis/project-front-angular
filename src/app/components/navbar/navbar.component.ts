import { Component, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Sidebar } from 'primeng/sidebar';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  items: MenuItem[] | undefined;
  visibleSideBar: boolean = false
  @ViewChild('sidebarRef') sidebarRef!: Sidebar;
  user: string | null | undefined;

  closeCallback(e: Event): void {
    this.sidebarRef.close(e);
  }

  sidebarVisible: boolean = false;

  ngOnInit() {
    this.user = localStorage.getItem("username");  
    this.items = [
      {
        label: 'Show Menu',
        icon: 'pi pi-bars'
      }
    ]
  }

}
