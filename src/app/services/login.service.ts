import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SignIn } from '../components/login/model/sign-in';
import { Observable, catchError } from 'rxjs';
import { LoginResponse } from '../components/login/model/login-response';
import { MessageService } from 'primeng/api';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  API_VALIDATE_USER = environment.API_VALIDATE_USER

  constructor(private http: HttpClient, private message: MessageService) { }

  validateUser(data: FormGroup): Observable<LoginResponse> {
    console.log("validateUser");    
    return this.http.post<LoginResponse>(this.API_VALIDATE_USER, data.value)
      .pipe(
        catchError((error:HttpErrorResponse) => {
          if (error.status == 422) {           
            throw  error.error.detail[0].msg; 
          }else{
            throw 'ocurrio un error inesperado!'; 
          }                   
        }))
  }
}
