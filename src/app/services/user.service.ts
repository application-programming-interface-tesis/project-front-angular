import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getData() {
    return [
      {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      },
      {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      },
      {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      },
      {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      }, {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      }, {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      }, {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      }, {
        fullName: "ASDS",
        userName: "SSDFDS",
        mail: "ASFDF@SFDD",
        rol: "ASF"
      }
    ]
  }

  getUsersLarge() {
    return Promise.resolve(this.getData().slice(0, 200));
  }
}
