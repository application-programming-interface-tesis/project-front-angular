import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulersConfigComponent } from './rulers-config.component';

describe('RulersConfigComponent', () => {
  let component: RulersConfigComponent;
  let fixture: ComponentFixture<RulersConfigComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RulersConfigComponent]
    });
    fixture = TestBed.createComponent(RulersConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
