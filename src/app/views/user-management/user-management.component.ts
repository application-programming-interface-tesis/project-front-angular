import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Toast, ToastModule } from 'primeng/toast';
import { UserService } from 'src/app/services/user.service';
import { userDto } from './models/userDto';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent {

  users!: userDto[]
  first = 0;
  rows = 10;

  constructor(private userService: UserService, private message: MessageService) {

  }

  ngOnInit() {
    this.getAllUser()
  }

  openRegisterUser() {
    
  }

  getAllUser() {
    this.userService.getUsersLarge().then((users) => (this.users = users));
    this.message.add({ severity: 'success', summary: 'Success', detail: 'usuarios obtenidos exitosamente' });
  }

  deleteUser() {

  }

  pageChange(event:any) {
    this.first = event.first;
    this.rows = event.rows;
  }
}
