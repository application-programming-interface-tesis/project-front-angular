// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  
  API_CREATE_USER:"http://localhost:8085//user/create_user",
  API_QUERY_USER:"http://localhost:8085/user/query_users",
  API_DELETE_USER:"http://localhost:8085/user/delete_user",
  API_VALIDATE_USER:"http://localhost:8085/user/validate_user",
  API_CAHNGE_PASSWORD:"http://localhost:8085/user/change_password",
  API_RESET_PASSWORD:"http://localhost:8085/user/reset_password",
 
};